using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public Transform playerTransform;

    float mouseSens = 300f;

    float cameraVertical = 0f;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {   

        // collect input
        float inputX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float inputY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        // rotate around x axis
        cameraVertical -= inputY;
        cameraVertical = Mathf.Clamp(cameraVertical, -90f, 90f);
        transform.localEulerAngles = Vector3.right * cameraVertical;

        // rotate around y axis
        playerTransform.Rotate(Vector3.up * inputX);


    }
}
