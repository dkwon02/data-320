using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class HiderAgent : Agent
{

    Rigidbody rb;

    float tempCounter = 0f;
    float rewardInterval = 5f;

    [SerializeField] int envNumber;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Initialize()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void OnEpisodeBegin()
    {
        transform.localRotation = Quaternion.Euler(0f, Random.Range(-180f, 180f), 0f);

        switch (envNumber)
        {
            case 1:
                transform.localPosition = new Vector3(Random.Range(-17f, -2f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 2:
                transform.localPosition = new Vector3(Random.Range(-17f, -13f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 3:
                transform.localPosition = new Vector3(Random.Range(-17f, -8f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 4:
                transform.localPosition = new Vector3(Random.Range(-12f, -10f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 5:
                transform.localPosition = new Vector3(Random.Range(-17f, 2f), 3.5f, Random.Range(-12f, -2f));
                break;
            case 6:
                transform.localPosition = new Vector3(Random.Range(-11f, -11f), 3.5f, Random.Range(1f, 13f));
                break;
            default:
                transform.localPosition = new Vector3(Random.Range(-52f, 52f), 3.5f, Random.Range(-16f, -10f));
                break;
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Seeker"))
        {
            AddReward(-5f);
            EndEpisode();
        }

        if (collision.gameObject.CompareTag("Border"))
        {
            AddReward(-0.1f);
            //EndEpisode();
        }
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float rotate = actions.ContinuousActions[0];
        float move = actions.ContinuousActions[1];

        rb.MovePosition(transform.position + transform.forward * move * 15f * Time.deltaTime);
        transform.Rotate(0f, rotate * 8f, 0f, Space.Self);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;

        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

    // Update is called once per frame
    void Update()
    {

        // add reward every 5 seconds for not being caught
        if (tempCounter <= 0) 
        {
            AddReward(2f);
            tempCounter = rewardInterval;
        }
        else
        {
            tempCounter -= Time.deltaTime;
        }

        if (transform.localPosition.y < -8f)
        {
            AddReward(-5f);
            EndEpisode();
        }

    }


}
