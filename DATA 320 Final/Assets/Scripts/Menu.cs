using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void PlayHider()
    {
        SceneManager.LoadScene(1);
    }

    public void PlaySeeker()
    {
        SceneManager.LoadScene(2);
    }

    public void Spectate()
    {
        SceneManager.LoadScene(3);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
