using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents.Integrations.Match3;
using UnityEngine;

public class SpectatorMove : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {

        //rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    { 

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Time.deltaTime * 20 * Vector3.forward);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Time.deltaTime * 20 * Vector3.back);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Time.deltaTime * 20 * Vector3.left);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Time.deltaTime * 20 * Vector3.right);
        }

        if (Input.GetKey(KeyCode.Z))
        {
            transform.Translate(Time.deltaTime * 5 * Vector3.up);
        }
        if (Input.GetKey(KeyCode.X))
        {
            transform.Translate(Time.deltaTime * 5 * Vector3.down);
        }
        
    }
}
