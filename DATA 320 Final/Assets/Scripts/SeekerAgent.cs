using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine.EventSystems;

public class SeekerAgent : Agent
{
    /**
     * +1 reward for each second it sees hider (using raycast)
     * 
     * +50 reward for contact with hider
     */

    [SerializeField] Transform targetTransform;

    [SerializeField] int envNumber;

    Rigidbody rb;

    void Start()
    {
        //transform.rotation = Quaternion.identity;
    }

    public override void Initialize()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float rotate = actions.ContinuousActions[0];
        float move = actions.ContinuousActions[1];

        rb.MovePosition(transform.position + transform.forward * move * 15f * Time.deltaTime);
        transform.Rotate(0f, rotate * 8, 0f, Space.Self);
    }

    public override void OnEpisodeBegin()
    {
        transform.localRotation = Quaternion.Euler(0f, Random.Range(-180f, 180f), 0f);

        switch (envNumber)
        {
            case 1:
                transform.localPosition = new Vector3(Random.Range(0f, 17f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 2:
                transform.localPosition = new Vector3(Random.Range(10f, 17f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 3:
                transform.localPosition = new Vector3(Random.Range(10f, 17f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 4:
                transform.localPosition = new Vector3(Random.Range(10f, 12f), 3.5f, Random.Range(-13f, 13f));
                break;
            case 5:
                transform.localPosition = new Vector3(Random.Range(2f, 17f), 3.5f, Random.Range(1f, 11f));
                break;
            case 6:
                transform.localPosition = new Vector3(Random.Range(-11f, 11f), 3.5f, Random.Range(-12f, -2f));
                break;
            default:
                transform.localPosition = new Vector3(Random.Range(-52f, 52f), 3.5f, Random.Range(10f, 16f));
                break;
        }
    }

    /**
     * Needs to know:
     * Its own location
     * Hider's location
     * Whether it sees the hider
     */
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;

        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Hider"))
        {
            //Debug.Log("hit hider");
            AddReward(5f);
            EndEpisode();
        }
        
        if (collision.gameObject.CompareTag("Border"))
        {
            AddReward(-1f);
            EndEpisode();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transform.localPosition.y < -8f)
        {
            AddReward(-5f);
            EndEpisode();
        }

    }
}
