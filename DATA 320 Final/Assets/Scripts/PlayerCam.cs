using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{

    public float sensX;
    public float sensY;

    public Transform orientation;

    float xRotate;
    float yRotate;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        // collect input
        float inputX = Input.GetAxisRaw("Mouse X") * sensX * Time.deltaTime;
        float inputY = Input.GetAxisRaw("Mouse Y") * sensY * Time.deltaTime;

        yRotate += inputX;
        xRotate -= inputY;

        xRotate = Mathf.Clamp(xRotate, 90f, -90f);

        // rotate camera and orientation
        transform.rotation = Quaternion.Euler(xRotate, yRotate, 0);
        orientation.rotation = Quaternion.Euler(0, yRotate, 0);
    }
}
